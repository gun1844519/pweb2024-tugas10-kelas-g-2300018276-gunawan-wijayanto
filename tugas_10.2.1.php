<!DOCTYPE html>
<html>
<head>
    <title>Konversi Nilai</title>
</head>
<body>
    <form method="post">
        <label for="nilai">Masukkan Nilai:</label>
        <input type="number" id="nilai" name="nilai" min="0" max="100" required>
        <input type="submit" value="Konversi">
    </form>


<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nilai = $_POST['nilai'];

    if ($nilai >= 80 && $nilai <= 100) {
        $nilaiHuruf = 'A';
    } elseif ($nilai >= 65 && $nilai < 80) {
        $nilaiHuruf = 'B';
    } elseif ($nilai >= 55 && $nilai < 65) {
        $nilaiHuruf = 'C';
    } elseif ($nilai >= 40 && $nilai < 55) {
        $nilaiHuruf = 'D';
    } else {
        $nilaiHuruf = 'E';
    }

    echo "Nilai Anda = $nilai, Konversi Nilai Huruf = $nilaiHuruf";
}
?>